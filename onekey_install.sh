#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
cat << "EOF"
╭━━━╮╱╱╱╱╱╱╱╱╱╱╭╮
┃╭━╮┃╱╱╱╱╱╱╱╱╱╱┃┃
┃╰━━┳━━┳━━┳━━┳━╯┣━━┳━╮
╰━━╮┃╭╮┃┃━┫┃━┫╭╮┃┃━┫╭╯
┃╰━╯┃╰╯┃┃━┫┃━┫╰╯┃┃━┫┃
╰━━━┫╭━┻━━┻━━┻━━┻━━┻╯
╱╱╱╱┃┃
╱╱╱╱╰╯

EOF
echo "Proxy node server installation script for CentOS 6 x64"
[ $(id -u) != "0" ] && { echo "${CFAILURE}Error: You must be root to run this script${CEND}"; exit 1; }
echo "Press Y for continue the installation process, or press any key else to exit."
read is_install
if [[ is_install =~ ^[Y,y,Yes,YES]$ ]]
then
	echo "Bye"
	exit 0
fi
echo "Updatin exsit package..."
yum clean all && rm -rf /var/cache/yum && yum update -y
echo "Install necessary package..."
yum install epel-release -y && yum makecache
wget https://gitlab.com/ifwang/onekey_install/raw/master/setuptools-0.6c11.tar.gz
tar zxvf setuptools-0.6c11.tar.gz && cd setuptools-0.6c11 && python setup.py install && cd ../
wget https://gitlab.com/ifwang/onekey_install/raw/master/pip-1.5.4.tar.gz && tar -zxvf pip-1.5.4.tar.gz && cd pip-1.5.4 && python setup.py install && cd ../ && pip install cymysql
yum -y groupinstall "Development Tools"
echo "Disabling iptables..."
service iptables stop && chkconfig iptables off
echo "Setting system timezone..."
timedatectl set-timezone Asia/Taipei && systemctl stop ntpd.service && ntpdate us.pool.ntp.org
echo "Installing libsodium..."
wget https://github.com/jedisct1/libsodium/releases/download/1.0.16/libsodium-1.0.16.tar.gz
tar xf libsodium-1.0.16.tar.gz && cd libsodium-1.0.16
./configure && make -j2 && make install
echo /usr/local/lib > /etc/ld.so.conf.d/usr_local_lib.conf
ldconfig
cd ../ && rm -rf libsodium*
echo "Installing Shadowsocksr server from GitHub..."
mkdir /soft && cd /soft
git clone -b manyuser https://gitlab.com/ifwang/shadowsocks.git
cd shadowsocks
echo "Generating config file..."
cp apiconfig.py userapiconfig.py
cp config.json user-config.json
sed -i -e "s/'modwebapi'/'glzjinmod'/g" userapiconfig.py
echo -n "Please enter database server's IP address:"
read db_ip
echo -n "DB name:"
read db_name
echo -n "DB username:"
read db_user
echo -n "DB password:"
read db_password
echo -n "Server node ID:"
read node_id
echo -n "Speedtest:"
read speedtest
echo "Writting config..."
sed -i -e "s/SPEEDTEST = 6/SPEEDTEST = ${speedtest}/g" -e "s/NODE_ID = 1/NODE_ID = ${node_id}/g" -e "s/MYSQL_HOST = '127.0.0.1'/MYSQL_HOST = '${db_ip}'/g" -e "s/MYSQL_USER = 'ss'/MYSQL_USER = '${db_user}'/g" -e "s/MYSQL_PASS = 'ss'/MYSQL_PASS = '${db_password}'/g" -e "s/MYSQL_DB = 'shadowsocks'/MYSQL_DB = '${db_name}'/g" userapiconfig.py
cat >> /etc/security/limits.conf << EOF
* soft nofile 51200
* hard nofile 51200
EOF
ulimit -n 51200
cat >> /etc/sysctl.conf << EOF
fs.file-max = 51200
net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
net.core.netdev_max_backlog = 250000
net.core.somaxconn = 4096
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.tcp_max_syn_backlog = 8192
net.ipv4.tcp_max_tw_buckets = 5000
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
net.ipv4.tcp_mtu_probing = 1
EOF
sysctl -p
rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm --quiet
yum install supervisor python-pip -y
pip install supervisor==3.1
chkconfig supervisord on
wget https://gitlab.com/ifwang/onekey_install/raw/master/supervisord.conf -O /etc/supervisord.conf
wget https://gitlab.com/ifwang/onekey_install/raw/master/supervisord -O /etc/init.d/supervisord
service supervisord start
echo "System require a reboot to complete the installation process, press Y to continue, or press any key else to exit this script."
read is_reboot
if [[ ${is_reboot} == "y" || ${is_reboot} == "Y" ]]; then
    reboot
else
    echo -e "${green}Info:${plain} Reboot has been canceled..."
    exit 0
fi